# Eris's Extended Binary Brainfck (Format)

An extended brainfuck interpreter.
Currently, development hasn't started.

## Goals
The main goal is to extend brainfck in to a more *complete* language,
with fancy features like functions, and unconditional jumps.
Effectively making it a lightweight VM.