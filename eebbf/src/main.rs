/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

const UNIMPLEMENTED_TEXT: &str = include_str!("unimplemented_text.txt");

fn main() {
    eprintln!("{}", UNIMPLEMENTED_TEXT);
}
