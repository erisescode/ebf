/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/// Holds expression information
pub mod expression;
use expression::Expr;
use std::iter::Iterator;

/// Parser, holds content as a [String].
#[derive(Clone)]
pub struct Parser {
    content: String,
    index: usize,
}

impl Parser {
    /// Creates a new parser with content.
    pub fn new(content: &str) -> Parser {
        Parser {
            content: content.into(),
            index: 0,
        }
    }

    /// Gets the next Expression in parser (same as [Parser::next])
    pub fn get_next_expr(&mut self) -> Option<Expr> {
        self.next()
    }

    /// Collects the expressions into a vector (same as [Parser::collect<Vec>])
    pub fn get_all_exprs(&mut self) -> Vec<Expr> {
        self.collect()
    }
}

impl Iterator for Parser {
    type Item = Expr;

    /// Gets the next Expression in parser content,
    /// Returns None at the end of iteration.
    ///
    /// # Example
    /// ```rust
    /// use ebf::parser::Parser;
    /// const CONTENT: &str = "+++---<><>";
    /// for e in Parser::new(CONTENT) {
    ///     println!("{e:?}");
    /// }
    /// ```
    fn next(&mut self) -> Option<Expr> {
        let Parser { content, index } = self;

        if *index == content.len() {
            None
        } else {
            let (e, s) = Expr::expr_from_str(&content[*index..]);
            *index += s;
            Some(e)
        }
    }
}
