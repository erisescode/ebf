/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/// An expression to be interpreted
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Expr {
    /// Adds *n* to the current cell (+ symbol repeated *n* times)
    Add(usize),
    /// Subtracts *n* from the current cell (- symbol repeated *n* times)
    Sub(usize),
    /// Moves the tape right *n* cells,
    /// ie increments the tape pointer (> repeated *n* times)
    Right(usize),
    /// Moves the tape left *n* cells, (< repeated *n* times)
    Left(usize),
    LoopOpen,
    LoopClose,
    /// Prints the current cell (. symbol)
    Print,
    /// Reads stdin to the current cell (, symbol)
    Read,
    /// Anything that isn't an expression
    NonExpr,
}

impl Expr {
    /// Returns an expression from a char and repetitions.
    fn expr_from_char(ch: char, x: usize) -> Expr {
        match ch {
            '+' => Expr::Add(x),
            '-' => Expr::Sub(x),
            '>' => Expr::Right(x),
            '<' => Expr::Left(x),
            '[' => Expr::LoopOpen,
            ']' => Expr::LoopClose,
            '.' => Expr::Print,
            ',' => Expr::Read,
            _ => Expr::NonExpr,
        }
    }

    /// Gets an expression from a string,
    /// returns expression and size of (for convenience).
    ///
    /// # Example
    /// ```rust
    /// use ebf::parser::expression::Expr;
    /// const TEST_STR: &str = "++++";
    /// let (e, size) = Expr::expr_from_str(TEST_STR);
    ///
    /// assert_eq!(size, 4);
    /// assert_eq!(e, Expr::Add(size));
    /// ```
    pub fn expr_from_str(s: &str) -> (Expr, usize) {
        // setup
        let mut chars = s.chars();
        let mut len = 1;
        let expr_type = chars.next().unwrap();

        // loop over all characters
        loop {
            let chr = chars.next();
            // break if end of string or if character is of different type
            if chr.is_none() || chr.unwrap() != expr_type {
                break;
            }

            len += 1;
        }

        // return len and expression
        (Self::expr_from_char(expr_type, len), len)
    }
}
