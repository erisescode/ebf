/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#[cfg(test)]
mod tests;

mod cell;
mod wrap;
//use wrap::*;

pub use cell::*;

#[derive(Copy, Clone)]
pub enum TapeType {
    Wrap,
    _Expand,
}

#[derive(Clone)]
pub struct Tape {
    _ttype: TapeType,
    index: usize,
    cells: Vec<Cell>,
}

impl Tape {
    pub fn new(ttype: TapeType) -> Tape {
        Tape {
            _ttype: ttype,
            index: 0,
            cells: Vec::new(),
        }
    }

    pub fn move_right(&mut self, amount: usize) {
        self.index += amount; //FIXME: Will overflow if index is too large
    }

    pub fn move_left(&mut self, amount: usize) {
        self.index -= amount; //FIXME: Will underflow if index - amount is less than 0
    }

    pub fn get_size(&self) -> usize {
        self.cells.len()
    }
    pub fn set_size(&mut self, size: usize) {
        self.cells.resize(size, Cell(0));
    }

    pub fn _get_type(&self) -> TapeType {
        self._ttype
    }
    pub fn _set_type(&mut self, ty: TapeType) {
        self._ttype = ty;
    }

    pub fn get_current_cell(&self) -> &Cell {
        &self.cells[self.index]
    }
    pub fn get_current_cell_mut(&mut self) -> &mut Cell {
        &mut self.cells[self.index]
    }
    pub fn set_current_cell(&mut self, to: Cell) {
        self.cells[self.index] = to;
    }
}
