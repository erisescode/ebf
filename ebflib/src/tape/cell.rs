/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use std::fmt::{Display, Formatter};
use std::ops::{Add, AddAssign, Sub, SubAssign};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct Cell(pub usize);

impl Display for Cell {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let Cell(v) = self;
        write!(f, "{}", v)
    }
}

impl Add<usize> for Cell {
    type Output = Cell;
    fn add(self, rhs: usize) -> Cell {
        let Cell(s) = self;
        Cell(s + rhs)
    }
}

impl AddAssign<usize> for Cell {
    fn add_assign(&mut self, rhs: usize) {
        *self = *self + rhs;
    }
}

impl Sub<usize> for Cell {
    type Output = Cell;
    fn sub(self, rhs: usize) -> Cell {
        let Cell(s) = self;
        Cell(s - rhs)
    }
}

impl SubAssign<usize> for Cell {
    fn sub_assign(&mut self, rhs: usize) {
        *self = *self - rhs;
    }
}
