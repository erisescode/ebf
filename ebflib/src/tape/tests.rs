/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::tape::*;

fn create_wrap_tape() -> Tape {
    Tape::new(TapeType::Wrap)
}

#[test]
fn test_basic_tape() {
    let mut tape = create_wrap_tape();
    tape.set_size(2);

    assert_eq!(*tape.get_current_cell(), Cell(0));
}

#[test]
fn test_basic_mutability() {
    const VALUE: Cell = Cell(300);

    let mut tape = create_wrap_tape();
    tape.set_size(1);

    tape.set_current_cell(VALUE);

    assert_eq!(*tape.get_current_cell(), VALUE);
}

#[test]
fn test_basic_movement() {
    const VALUE_1: Cell = Cell(20);
    const VALUE_2: Cell = Cell(50);

    let mut tape = create_wrap_tape();
    tape.set_size(2);

    tape.set_current_cell(VALUE_1);
    tape.move_right(1);

    tape.set_current_cell(VALUE_2);
    tape.move_left(1);

    assert_eq!(*tape.get_current_cell(), VALUE_1);
    tape.move_right(1);
    assert_eq!(*tape.get_current_cell(), VALUE_2);
}
