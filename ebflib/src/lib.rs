/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/// Module containing structs and functions pertaining to parsing text into expressions.
pub mod parser;

/// Module containing tape, and tape related functions.
pub mod tape;

/// Module containing structs and classes relating to interpreting expressions ([ebf::parser::expression]).
pub mod interpreter;