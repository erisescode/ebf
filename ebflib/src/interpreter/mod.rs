/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use std::default::Default;

use std::io::{self, Read};

use crate::parser::expression::Expr;
use crate::tape::TapeType;
use crate::tape::{Cell, Tape};

/// Tests for the interpreter.
#[cfg(test)]
mod tests;

/// An Interpreter.
/// Holds expressions and state internally.
///
/// To construct an interpreter, use [InterpreterBuilder].
pub struct Interpreter {
    expr: Vec<Expr>,
    index: usize,
    loop_stack: Vec<usize>,
    tape: Tape,
}

impl Interpreter {
    /// Skips to the end of a loop
    fn skip_loop(&mut self) {
        let mut loop_counter = 0;
        let mut index = self.index;

        // loop until loop_counter = 0
        loop {
            // current expression
            let e = self.expr[index];

            // increment/decrement loop_counter, on [ or ] respectively
            match e {
                Expr::LoopOpen => loop_counter += 1,
                Expr::LoopClose => loop_counter -= 1,
                _ => {} // do nothing otherwise
            }
            // increment index
            index += 1;

            if loop_counter == 0 {
                break;
            }
        }

        // set index to final index
        self.index = index;
    }

    /// adds a loop to the interpreter
    fn add_loop(&mut self) {
        // skip to end of loop if value is 0
        if *self.tape.get_current_cell() == Cell(0) {
            self.skip_loop();
        } else {
            // pushes to stack
            self.loop_stack.push(self.index);
        }
    }

    /// Either continues and pops the loop or goes to the start of the loop.
    fn goto_loop(&mut self) {
        // continue and pop
        if *self.tape.get_current_cell() == Cell(0) {
            self.loop_stack.pop();
        } else {
            // go back to loop start
            let loc = self.loop_stack.last().unwrap();
            self.index = *loc;
        }
    }

    fn read_byte_stdin(&mut self) {
        let byte = io::stdin().bytes().next().unwrap_or(Ok(0)).unwrap();

        let cell = Cell(byte as usize);
        self.tape.set_current_cell(cell);
    }

    /// Interprets a single expression,
    /// Useful for debugging.
    ///
    /// # Example
    /// ```rust
    /// use ebf::interpreter::InterpreterBuilder;
    /// use ebf::parser::expression::Expr;
    /// use ebf::tape::Cell;
    ///
    /// let mut interpreter = InterpreterBuilder::default()
    ///     .with_tape_size(1)
    ///     .build();
    ///
    /// interpreter.interpret_expr(Expr::Add(72)); // adds 72 (ASCII/UTF-8 'A') to current cell.
    /// assert_eq!(*interpreter.get_tape().get_current_cell(), Cell(72));
    /// ```
    pub fn interpret_expr(&mut self, e: Expr) {
        match e {
            Expr::Add(x) => *self.tape.get_current_cell_mut() += x,
            Expr::Sub(x) => *self.tape.get_current_cell_mut() -= x,
            Expr::Right(x) => self.tape.move_right(x),
            Expr::Left(x) => self.tape.move_left(x),
            Expr::LoopOpen => self.add_loop(),
            Expr::LoopClose => self.goto_loop(),
            Expr::Print => {
                let Cell(c) = self.tape.get_current_cell();
                print!("{}", char::from_u32(*c as u32).unwrap());
            }
            Expr::Read => self.read_byte_stdin(),
            _ => {}
        }
    }

    /// Interprets all expressions in self.
    /// Equivalent to:
    /// ```rust
    /// # use ebf::interpreter::InterpreterBuilder;
    /// # let mut interpreter = InterpreterBuilder::default().build();
    /// for expr in interpreter.get_exprs().clone() {
    ///     interpreter.interpret_expr(expr);
    ///  }
    /// ```
    pub fn interpret(&mut self) {
        while self.index < self.expr.len() {
            self.interpret_expr(self.expr[self.index]);
            self.index += 1;
        }
    }

    /// Returns the expressions in self.
    pub fn get_exprs(&self) -> &Vec<Expr> {
        &self.expr
    }

    /// Gets the tape of the interpreter.
    ///
    /// # Example
    /// ```rust
    /// # use ebf::interpreter::InterpreterBuilder;
    /// use ebf::tape::Cell;
    /// # let mut interpreter = InterpreterBuilder::default()
    /// # .with_tape_size(1)
    /// # .build();
    ///
    /// interpreter.interpret();
    /// let Cell(c) = interpreter.get_tape().get_current_cell();
    /// println!("{c}");
    /// ```
    pub fn get_tape(&self) -> &Tape {
        &self.tape
    }

    pub fn set_tape(&mut self, tape: Tape) {
        self.tape = tape;
    }
}

/// Builder for [Interpreter]
#[derive(Clone)]
pub struct InterpreterBuilder {
    expr: Vec<Expr>,
    tape: Tape,
}

impl InterpreterBuilder {
    /// Creates a new InterpreterBuilder
    pub fn new() -> Self {
        Self {
            expr: vec![],
            tape: Tape::new(TapeType::Wrap),
        }
    }

    /// Sets the tape of the Interpreter.
    ///
    /// Also see [InterpreterBuilder::with_tape_type] and [InterpreterBuilder::with_tape_size]
    ///
    /// # Example
    /// ```rust
    /// use ebf::interpreter::InterpreterBuilder;
    /// use ebf::parser::expression::Expr;
    /// use ebf::tape::{Cell, Tape, TapeType};
    ///
    /// // create tape
    /// let mut tape = Tape::new(TapeType::Wrap);
    /// tape.set_size(1); // very small size
    /// tape.set_current_cell(Cell(5));
    ///
    /// // create interpreter
    /// let mut interp = InterpreterBuilder::new()
    ///     .with_tape(tape)
    ///     .build();
    ///
    /// // asserts that cell actually has 5 in it.
    /// assert_eq!(*interp.get_tape().get_current_cell(), Cell(5));
    /// ```
    pub fn with_tape(self, tape: Tape) -> Self {
        Self {
            expr: self.expr,
            tape,
        }
    }

    /// Sets the tape size of the Interpreter.
    ///
    /// # Example
    /// ```rust
    /// use ebf::interpreter::InterpreterBuilder;
    /// let mut interp = InterpreterBuilder::new()
    ///     .with_tape_size(10) // sets tape size to 10
    ///     .build();
    ///
    /// // assert length as 10
    /// assert_eq!(interp.get_tape().get_size(), 10);
    /// ```
    pub fn with_tape_size(self, size: usize) -> Self {
        let mut tape = Tape::new(self.tape._get_type());
        tape.set_size(size);

        Self {
            expr: self.expr,
            tape,
        }
    }

    /// Sets the tape type,
    /// Currently unused.
    pub fn with_tape_type(self, ty: TapeType) -> Self {
        let mut tape = self.tape;
        tape._set_type(ty);
        Self {
            expr: self.expr,
            tape,
        }
    }

    /// Sets the expressions in the interpreter to a slice.
    ///
    /// # Example
    /// ```rust
    /// use ebf::interpreter::InterpreterBuilder;
    /// use ebf::parser::expression::Expr;
    /// # use ebf::tape::Cell;
    ///
    /// const EXPR_SLICE: &[Expr] = &[Expr::Add(50), Expr::Print];
    ///
    /// let mut interp = InterpreterBuilder::new()
    ///     .with_tape_size(2) // size 2 due to the movement right
    ///     .with_expr(EXPR_SLICE)
    ///     .build();
    ///  interp.interpret(); // prints '2' (ASCII 50)
    /// # assert_eq!(*interp.get_tape().get_current_cell(), Cell(50));
    /// ```
    pub fn with_expr(self, expr: &[Expr]) -> Self {
        let expr = Vec::from(expr);
        Self {
            expr,
            tape: self.tape,
        }
    }

    /// Builds the InterpreterBuilder into an [Interpreter]
    pub fn build(self) -> Interpreter {
        Interpreter {
            expr: self.expr,
            index: 0,
            loop_stack: vec![],
            tape: self.tape,
        }
    }
}

impl Default for InterpreterBuilder {
    /// Default for [InterpreterBuilder]
    /// Same as `InterpreterBuilder::new()`
    fn default() -> Self {
        Self::new()
    }
}
