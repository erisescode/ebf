/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::interpreter::*;
use crate::tape::Cell;

#[test]
fn basic_expr_test() {
    const EXPRS: [Expr; 4] = [Expr::Add(2), Expr::Right(1), Expr::Add(5), Expr::Left(1)];
    let mut interp = InterpreterBuilder::new().with_tape_size(8).build();

    for e in EXPRS {
        interp.interpret_expr(e);
    }

    let mut tape = interp.get_tape().clone();

    assert_eq!(*tape.get_current_cell(), Cell(2));
    tape.move_right(1);
    assert_eq!(*tape.get_current_cell(), Cell(5));
}
