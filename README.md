# Eris's Brainfck interpreter.

Is a brainfck interpreter, with good performance.
It's also entirely written in safe*ish* rust.
(Only safe*ish* due to index out of bounds exceptions).

Currently there are three sub-projects, [ebf (library)](./ebflib), [ebf (binary)](./ebfbin) and [eebbf](./eebbf),
ebf is a minimal *strictly* brainfck interpreter, with no extensions.
eebbf is an extended version of ebf.
eebbf uses ebf as it's core interpreter.

All code in ebf is licensed under the [Mozilla Public License version 2.0](https://www.mozilla.org/en-US/MPL/2.0/),
and is FOSS software.

## NOTE:
Currently a W.I.P project.
