# Todo's
Other than one's in particular files.

* Add documentation
* Add `set_tape(...)` to [Interpreter](../ebflib/src/interpreter/mod.rs)
* Add `append_expr(...)` and `set_exprs(...)` to [Interpreter](../ebflib/src/interpreter/mod.rs)
* Add `#[derive(..., Eq)]` to [Cell](../ebflib/src/tape/cell.rs)
* Add Tape Types