/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// extern imports
use std::fs;
use clap::Parser;

// crate imports
use ebf;
use ebf::interpreter::InterpreterBuilder;
use ebf::parser::expression::Expr;

// TODO: change this
const DEFAULT_TAPE_SIZE: usize = 64;

#[derive(clap::Parser, Debug)]
#[clap(author, version, about)]
struct Arguments {
    #[clap(help = "File to interpret")]
    file: String,

    #[clap(short='s', long, default_value_t=DEFAULT_TAPE_SIZE)]
    tape_size: usize,
    // TODO: add tape type
}

fn main() -> std::io::Result<()> {
    let args = <Arguments as clap::Parser>::parse();

    let s = fs::read_to_string(args.file)?;
    let exprs = ebf::parser::Parser::new(&s).collect::<Vec<Expr>>();

    let mut i = InterpreterBuilder::new()
        .with_tape_size(args.tape_size)
        .with_expr(exprs.as_slice())
        .build();
    i.interpret();

    Ok(())
}
