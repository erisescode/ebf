# Contributing Guide.

EBF is FOSS (MPL-v2.0) software and is always looking for new contributions,
if you have a contribution (a bug fix, a new feature, etc.), 
add a [Gitlab Issue](https://docs.gitlab.com/ee/user/project/issues/), 
and or merge request.

## When contributing:
* Create a new (temporary) branch for your code, do **not** push to main.
* Remember that EBF is licensed under the Mozilla Public License, 
  which requires [this snippet](license-header.txt) (or similar) at the start of every file.
* Be patient and kind with any other contributors.

## Before creating a pull request:
* Squash commits down to a reasonable amount.
* run `cargo fmt` for consistent formatting.
* run `cargo clippy`, and fix errors and warnings, 
  except for dead code if it's exposed in [src/lib.rs](src/lib.rs).

## Tests
Writing tests is important for testing specific pieces of functionality,
they help reduce bugs and generally are good.

For most functions you should write some kind of test that incorporates them, 
and due to rust, it's not very hard to write tests.
See [here](https://doc.rust-lang.org/book/ch11-01-writing-tests.html) for the rust documentation on tests.
A lot of the tests in this project are [documentation tests](https://doc.rust-lang.org/rustdoc/write-documentation/documentation-tests.html),
to reduce code size, and to provide [examples](#examples).

# Documentation

Documentation is important to any project.
If you write code, 
it would be of great help if you can document it.
Documentation should explain the functions purpose,
and it's parameters if not already obvious.
Documentation is written using [rustdoc](https://doc.rust-lang.org/rustdoc/what-is-rustdoc.html)

## Examples
Documentation should also provide an example of usage,
this can be used as a [test](#tests) as well.

## Example documentation comment.
```rust
/// My function does stuff,
/// It takes some arguments,
/// It returns a value.
///
/// # Example
/// ```
/// // Example/test goes here
/// // this code will be run when `cargo test` is run.
/// ```
pub fn my_func(/* args */) {
  /* code */
}
```
